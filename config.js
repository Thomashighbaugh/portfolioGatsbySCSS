'use strict';
module.exports = {
  url: 'https://thomasleonhighbaugh.me',
  title: 'Development Portfolio',
  tagline:
    'Autodidactic web developer with a passion for Linux and a flare for design.',
  copyright: '© 2019 Thomas Leon Highbaugh, All rights reserved',
  author: {
    name: 'Thomas Leon Highbaugh',
    bio: 'Self-taught web developer and whisky aficionado',
    contacts: {
      linkedin: 'https://www.linkedin.com/in/thomas-leon-highbaugh',
      github: 'https://github.com/Thomashighbaugh',
      gitlab: 'https://github.com/thomasleonhighbaugh',
      twitter: 'https://twitter.com/thomasleonhighbaugh',
      instagram: 'https://instagram.com/tlh-resurgens',
      dribbble: 'https://dribbble.com/thighbaugh',
    },
  },
};
