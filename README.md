<div align="center" style="background-color: #343a40; outline: #669933 solid 5px;">
  <a href="https://www.gatsbyjs.org">
    <img alt="Gatsby" src="https://www.gatsbyjs.org/monogram.svg" width="200" />
  </a>
</div>
<h1 align="center">
  Thomas Leon Highbaugh's Web Development Portfolio
</h1>

# Thomas Leon Highbaugh Portfolio

> The portfolio site of Thomas Leon Highbaugh built with React, Redux, Webpack and SASS in a structured, modular form.

This repository serves to host the source code of the developer portfolio of Thomas Leon Highbaugh. The site that is presented here is built primarily with React, thus serving as a living testament to the web development prowess of TLH, and includes functionality from Redux, SASS and WebPack to improve the site's UI and DX (developer experience).

This site replaces TLH's former portfolio, Resurgens, and its associated branding as a means of demonstrating the increasing maturity of TLH as a developer while conforming it more to the traditional portfolio site's standards without compromising in regards to its style.
![](screen.png)
