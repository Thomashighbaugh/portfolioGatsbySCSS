(window.webpackJsonp = window.webpackJsonp || []).push([
  [8],
  {
    273: function(e, t, a) {
      'use strict';
      a.r(t);
      var n = a(0),
        l = a.n(n),
        r = a(266),
        i = a(328),
        s = a.n(i),
        c = (a(329), a(267)),
        o = a(285),
        m = a.n(o);
      var u = (function(e) {
        var t, a;
        function n() {
          return e.apply(this, arguments) || this;
        }
        return (
          (a = e),
          ((t = n).prototype = Object.create(a.prototype)),
          (t.prototype.constructor = t),
          (t.__proto__ = a),
          (n.prototype.render = function() {
            return l.a.createElement(
              r.a,
              null,
              l.a.createElement(c.a, {
                title: '[resume]',
                description:
                  'created from the output of JSONresume into HTML adapted into JSX, this is the live resume of TLH',
              }),
              l.a.createElement(
                m.a,
                null,
                l.a.createElement(
                  'div',
                  { id: 'resume' },
                  l.a.createElement(
                    'div',
                    { className: 'card-resume' },
                    l.a.createElement(
                      'section',
                      null,
                      l.a.createElement(
                        'div',
                        { className: 'resume-head col-10 mt-5 offset-1' },
                        l.a.createElement(
                          'h1',
                          { className: 'card-title font-weight-bold col-9' },
                          'Thomas Leon Highbaugh'
                        ),
                        l.a.createElement(
                          'h2',
                          { className: 'card-subtitle ml-4 col-11' },
                          'Software Engineer & Freelance IT Professional'
                        )
                      ),
                      l.a.createElement(
                        'div',
                        { id: 'content', className: 'card-body offset-2' },
                        l.a.createElement(
                          'section',
                          { id: 'basics' },
                          l.a.createElement(
                            'div',
                            { className: 'contact card-title' },
                            l.a.createElement(
                              'h2',
                              { className: 'card-subtitle' },
                              'Contact'
                            ),
                            l.a.createElement(
                              'div',
                              { className: 'contact' },
                              l.a.createElement('strong', null, 'Website: '),
                              l.a.createElement(
                                'a',
                                { href: 'http://thomasleonhighbaugh.me' },
                                'thomasleonhighbaugh.me'
                              )
                            ),
                            l.a.createElement(
                              'div',
                              { className: 'contact' },
                              l.a.createElement('strong', null, 'Email: '),
                              l.a.createElement(s.a, {
                                email: 'thighbaugh@zoho.com',
                              })
                            ),
                            l.a.createElement(
                              'div',
                              { className: 'contact' },
                              l.a.createElement('strong', null, 'Phone: '),
                              l.a.createElement(s.a, { tel: '510-907-0654' })
                            )
                          ),
                          l.a.createElement(
                            'div',
                            { className: 'summary' },
                            l.a.createElement('h3', null, 'About'),
                            l.a.createElement(
                              'p',
                              null,
                              'A self-taught software developer with an eye for design and a passion for Linux.'
                            )
                          )
                        ),
                        l.a.createElement(
                          'section',
                          { className: 'btn-group', id: 'profiles' },
                          l.a.createElement(
                            'h2',
                            { className: 'card-subtitle' },
                            'Internet Profiles & Personal Sites'
                          ),
                          l.a.createElement(
                            'a',
                            {
                              className: 'nav-btn',
                              href:
                                'http://linkedin.com/in/thomas-leon-highbaugh',
                            },
                            'LinkedIn'
                          ),
                          l.a.createElement(
                            'a',
                            {
                              className: 'nav-btn',
                              href: 'https://github.com/Thomashighbaugh',
                            },
                            'GitHub'
                          ),
                          l.a.createElement(
                            'a',
                            {
                              className: 'nav-btn',
                              href: 'https://dribbble.com/thighbaugh',
                            },
                            'Dribbble'
                          ),
                          l.a.createElement(
                            'a',
                            {
                              className: 'nav-btn',
                              href: 'https://gitlab.com/thighbaugh',
                            },
                            'GitLab'
                          ),
                          l.a.createElement(
                            'a',
                            {
                              className: 'nav-btn',
                              href: 'https://instagram.com/tlh-resurgens',
                            },
                            'Instagram'
                          ),
                          l.a.createElement(
                            'a',
                            {
                              className: 'nav-btn',
                              href: 'https://gitlab.com/thighbaugh',
                            },
                            'Blog                  '
                          ),
                          l.a.createElement(
                            'a',
                            {
                              className: 'nav-btn',
                              href: 'https://gitlab.com/thighbaugh',
                            },
                            'Gallery                  '
                          )
                        ),
                        l.a.createElement(
                          'section',
                          { className: 'col-10', id: 'work' },
                          l.a.createElement(
                            'h2',
                            {
                              className:
                                'card-title font-weight-bold resume-head',
                            },
                            'Work'
                          ),
                          l.a.createElement(
                            'div',
                            { className: 'item' },
                            l.a.createElement(
                              'h3',
                              { className: 'work_name' },
                              'Freelance'
                            ),
                            l.a.createElement(
                              'div',
                              { className: 'work_position' },
                              'Web Developer'
                            ),
                            l.a.createElement(
                              'div',
                              { className: 'work_date' },
                              l.a.createElement(
                                'span',
                                { className: 'startDate' },
                                '2018-03-15'
                              ),
                              l.a.createElement(
                                'span',
                                { className: 'endDate' },
                                '- present'
                              )
                            ),
                            l.a.createElement(
                              'div',
                              { className: 'work_website' },
                              l.a.createElement(
                                'a',
                                {
                                  className: 'nav-btn',
                                  href: 'https://thomasleonhighbaugh.me',
                                },
                                'https://thomasleonhighbaugh.me'
                              )
                            ),
                            l.a.createElement(
                              'ul',
                              { className: 'highlights' },
                              l.a.createElement(
                                'li',
                                null,
                                'Designing and coding websites for customers from the ground up'
                              ),
                              l.a.createElement(
                                'li',
                                null,
                                'Developing customer relationships and responding to customer inquiries'
                              ),
                              l.a.createElement(
                                'li',
                                null,
                                'Keeping ahead of industry trends while continuing education about web-related technologies'
                              ),
                              l.a.createElement(
                                'li',
                                null,
                                'Vigilant market research and analysis of the competition'
                              )
                            )
                          ),
                          l.a.createElement('hr', null),
                          l.a.createElement(
                            'div',
                            { className: 'item' },
                            l.a.createElement(
                              'h3',
                              { className: 'work_name' },
                              'Freelance'
                            ),
                            l.a.createElement(
                              'div',
                              { className: 'work_position' },
                              'Computer Repair Technician'
                            ),
                            l.a.createElement(
                              'div',
                              { className: 'work_date' },
                              l.a.createElement(
                                'span',
                                { className: 'startDate' },
                                '2018-02-18'
                              ),
                              l.a.createElement(
                                'span',
                                { className: 'endDate' },
                                '- present'
                              )
                            ),
                            l.a.createElement(
                              'div',
                              { className: 'work_website' },
                              l.a.createElement(
                                'a',
                                {
                                  className: 'nav-btn',
                                  href: 'https://thomasleonhighbaugh.me',
                                },
                                'https://thomasleonhighbaugh.me'
                              )
                            ),
                            l.a.createElement(
                              'ul',
                              { className: 'highlights' },
                              l.a.createElement(
                                'li',
                                null,
                                'Troubleshooting PCs that are not performing as expected'
                              ),
                              l.a.createElement(
                                'li',
                                null,
                                'Educating customers about routine PC maintanence and security'
                              ),
                              l.a.createElement(
                                'li',
                                null,
                                'Designing and implementing PC builds based on customer budgets and desired use cases'
                              ),
                              l.a.createElement(
                                'li',
                                null,
                                'Marketing and customer relationship development to maintain stream of work'
                              ),
                              l.a.createElement(
                                'li',
                                null,
                                'Learning and keeping tabs on personal and commercial electronic markets'
                              ),
                              l.a.createElement(
                                'li',
                                null,
                                'Market research to keep abilities in sync with customer needs'
                              )
                            )
                          ),
                          l.a.createElement('hr', null),
                          l.a.createElement(
                            'div',
                            { className: 'item' },
                            l.a.createElement(
                              'h3',
                              { className: 'work_name' },
                              'Pet Food Express: Blackhawk'
                            ),
                            l.a.createElement(
                              'div',
                              { className: 'work_position' },
                              'Assistant Manager IV'
                            ),
                            l.a.createElement(
                              'div',
                              { className: 'work_date' },
                              l.a.createElement(
                                'span',
                                { className: 'startDate' },
                                '2012-05-13'
                              ),
                              l.a.createElement(
                                'span',
                                { className: 'endDate' },
                                '- 2018-04-19'
                              )
                            ),
                            l.a.createElement(
                              'div',
                              { className: 'work_website' },
                              l.a.createElement(
                                'a',
                                {
                                  className: 'nav-btn',
                                  href: 'https://petfoodexpress.com',
                                },
                                'https://petfoodexpress.com'
                              )
                            ),
                            l.a.createElement(
                              'ul',
                              { className: 'highlights' },
                              l.a.createElement(
                                'li',
                                null,
                                'Educating customers about holistic pet foods and how to appropriately use them'
                              ),
                              l.a.createElement(
                                'li',
                                null,
                                'Staff development and consultation to ensure corporate standards are complied with'
                              ),
                              l.a.createElement(
                                'li',
                                null,
                                'Setting mood of the staff and maintaining a customer-first atmosphere'
                              ),
                              l.a.createElement(
                                'li',
                                null,
                                'Assisting store manager in daily cash and store operations'
                              ),
                              l.a.createElement(
                                'li',
                                null,
                                'Ensuring store is tidy and stocked at all times'
                              ),
                              l.a.createElement(
                                'li',
                                null,
                                'Managing customer comments and concerns with sensitivity and promptness'
                              )
                            )
                          )
                        ),
                        l.a.createElement('hr', {
                          className: 'hr offset-2 col-8',
                        }),
                        l.a.createElement(
                          'section',
                          { className: 'col-10', id: 'volunteer' },
                          l.a.createElement(
                            'h2',
                            {
                              className:
                                'card-title font-weight-bold resume-head',
                            },
                            'Volunteer'
                          ),
                          l.a.createElement(
                            'div',
                            { className: 'item' },
                            l.a.createElement(
                              'h3',
                              { className: 'company' },
                              'South Hayward Parish'
                            ),
                            l.a.createElement(
                              'div',
                              { className: 'work_position' },
                              'Volunteer'
                            ),
                            l.a.createElement(
                              'div',
                              { className: 'summary' },
                              l.a.createElement(
                                'p',
                                null,
                                'A small homeless shelter in Hayward, California that leverages volunteer work and community donations to help the needy'
                              )
                            ),
                            l.a.createElement(
                              'ul',
                              { className: 'highlights' },
                              l.a.createElement(
                                'li',
                                null,
                                'Assisted in rehabilitating old PCs for resident use in job seeking'
                              ),
                              l.a.createElement(
                                'li',
                                null,
                                'Stocking kitchen with fresh donations from local grocery stores'
                              ),
                              l.a.createElement(
                                'li',
                                null,
                                'Assisting residents in using PCs to find jobs'
                              ),
                              l.a.createElement(
                                'li',
                                null,
                                'Talking to residents and relaying positive messages to keep them motivated'
                              )
                            )
                          )
                        ),
                        l.a.createElement('hr', {
                          className: 'hr offset-2 col-8',
                        }),
                        l.a.createElement(
                          'section',
                          { className: 'col-10', id: 'education' },
                          l.a.createElement(
                            'h2',
                            {
                              className:
                                'card-title font-weight-bold resume-head',
                            },
                            'Education'
                          ),
                          l.a.createElement(
                            'div',
                            { className: 'item' },
                            l.a.createElement(
                              'div',
                              { className: 'institution' },
                              'Codify Academy'
                            ),
                            l.a.createElement(
                              'div',
                              { className: 'study_date' },
                              l.a.createElement(
                                'span',
                                { className: 'startDate' },
                                '2018-02-14'
                              ),
                              l.a.createElement(
                                'span',
                                { className: 'endDate' },
                                '- 2018-05-30'
                              )
                            ),
                            l.a.createElement(
                              'ul',
                              { className: 'courses' },
                              l.a.createElement('li', null, 'HTML5'),
                              l.a.createElement('li', null, 'CSS3'),
                              l.a.createElement('li', null, 'Javascript'),
                              l.a.createElement('li', null, 'Vue'),
                              l.a.createElement('li', null, 'Restful APIs'),
                              l.a.createElement('li', null, 'Front End Design'),
                              l.a.createElement('li', null, 'Git'),
                              l.a.createElement(
                                'li',
                                null,
                                'Web Development Workflows'
                              )
                            ),
                            l.a.createElement(
                              'div',
                              { className: 'gpa' },
                              l.a.createElement('span', null, 'GPA 4.0')
                            )
                          ),
                          l.a.createElement('hr', null),
                          l.a.createElement(
                            'div',
                            { className: 'item' },
                            l.a.createElement(
                              'div',
                              { className: 'institution' },
                              'Las Positas/Chabot Community College'
                            ),
                            l.a.createElement(
                              'div',
                              { className: 'study_date' },
                              l.a.createElement(
                                'span',
                                { className: 'startDate' },
                                '2018-02-14'
                              ),
                              l.a.createElement(
                                'span',
                                { className: 'endDate' },
                                '- 2018-05-30'
                              )
                            ),
                            l.a.createElement(
                              'ul',
                              { className: 'courses' },
                              l.a.createElement('li', null, 'HTML5/CSS3'),
                              l.a.createElement('li', null, 'Linux'),
                              l.a.createElement('li', null, 'C++'),
                              l.a.createElement('li', null, 'Java'),
                              l.a.createElement('li', null, 'Javascript'),
                              l.a.createElement('li', null, 'Ethical Hacking')
                            ),
                            l.a.createElement(
                              'div',
                              { className: 'gpa' },
                              l.a.createElement('span', null, ' GPA 4.0')
                            )
                          ),
                          l.a.createElement('hr', null),
                          l.a.createElement(
                            'div',
                            { className: 'item' },
                            l.a.createElement(
                              'div',
                              { className: 'institution' },
                              'California State University East Bay (unrelated major)'
                            ),
                            l.a.createElement(
                              'div',
                              { className: 'study_date' },
                              l.a.createElement(
                                'span',
                                { className: 'startDate' },
                                '2009-09'
                              ),
                              l.a.createElement(
                                'span',
                                { className: 'endDate' },
                                '- 2014-04'
                              )
                            ),
                            l.a.createElement(
                              'ul',
                              { className: 'courses' },
                              l.a.createElement('li', null, 'C++'),
                              l.a.createElement('li', null, 'Linux'),
                              l.a.createElement('li', null, 'Java'),
                              l.a.createElement('li', null, 'Logic'),
                              l.a.createElement('li', null, 'Economics (Macro)')
                            ),
                            l.a.createElement(
                              'div',
                              { className: 'gpa' },
                              l.a.createElement('span', null, ' GPA 3.7')
                            )
                          )
                        ),
                        l.a.createElement('hr', {
                          className: 'hr offset-2 col-8',
                        }),
                        l.a.createElement(
                          'section',
                          { id: 'skills', className: 'flex-column col-10' },
                          l.a.createElement(
                            'h2',
                            {
                              className:
                                'card-title font-weight-bold resume-head',
                            },
                            'Skills'
                          ),
                          l.a.createElement(
                            'div',
                            { className: 'item' },
                            l.a.createElement(
                              'div',
                              { className: 'card-title font-weight-bold' },
                              'Web Development'
                            ),
                            l.a.createElement(
                              'ul',
                              { className: 'keywords' },
                              l.a.createElement('li', null, 'HTML5 & CSS3'),
                              l.a.createElement(
                                'li',
                                null,
                                'Javascript, JSX, Node & React'
                              ),
                              l.a.createElement(
                                'li',
                                null,
                                'PHP, LAMP & MySQL'
                              ),
                              l.a.createElement('li', null, 'Python & Perl'),
                              l.a.createElement(
                                'li',
                                null,
                                'SASS, SCSS & Less'
                              ),
                              l.a.createElement(
                                'li',
                                null,
                                'BASH, ZSH & Shell Scripting'
                              )
                            )
                          ),
                          l.a.createElement(
                            'div',
                            { className: 'item' },
                            l.a.createElement(
                              'div',
                              { className: 'card-title font-weight-bold' },
                              'Linux Systems Administration'
                            ),
                            l.a.createElement(
                              'ul',
                              { className: 'keywords' },
                              l.a.createElement('li', null, 'BASH'),
                              l.a.createElement('li', null, 'ZSH'),
                              l.a.createElement('li', null, 'Linux'),
                              l.a.createElement('li', null, 'Ansible'),
                              l.a.createElement('li', null, 'SSH'),
                              l.a.createElement('li', null, 'Virtualization'),
                              l.a.createElement('li', null, 'Containers')
                            )
                          ),
                          l.a.createElement(
                            'div',
                            { className: 'item' },
                            l.a.createElement(
                              'div',
                              { className: 'card-title font-weight-bold' },
                              'Hardware Configuration & Repair'
                            ),
                            l.a.createElement(
                              'ul',
                              { className: 'keywords' },
                              l.a.createElement('li', null, 'Windows'),
                              l.a.createElement('li', null, 'macOS'),
                              l.a.createElement('li', null, 'Linux'),
                              l.a.createElement('li', null, 'Server'),
                              l.a.createElement(
                                'li',
                                null,
                                'Component Installation'
                              ),
                              l.a.createElement('li', null, 'Troubleshooting')
                            )
                          ),
                          ' ',
                          l.a.createElement(
                            'div',
                            { className: 'item' },
                            l.a.createElement(
                              'div',
                              { className: 'card-title font-weight-bold' },
                              ' ',
                              'Linux Development',
                              ' '
                            ),
                            l.a.createElement(
                              'ul',
                              { className: 'keywords' },
                              l.a.createElement(
                                'li',
                                null,
                                'BASH Shell Scripting'
                              ),
                              l.a.createElement(
                                'li',
                                null,
                                'Electron Platform'
                              ),
                              l.a.createElement(
                                'li',
                                null,
                                'Linux Configuration (aka dotfiles)'
                              ),
                              l.a.createElement('li', null, 'C++')
                            )
                          )
                        ),
                        l.a.createElement('hr', {
                          className: 'col-8 offset-2 d-block hr',
                        }),
                        l.a.createElement(
                          'section',
                          { id: 'languages', className: 'flex-column col-10' },
                          l.a.createElement(
                            'h2',
                            {
                              className:
                                'card-title font-weight-bold resume-head',
                            },
                            'Languages'
                          ),
                          l.a.createElement(
                            'div',
                            { className: 'item' },
                            l.a.createElement(
                              'div',
                              { className: 'language card-title' },
                              'English'
                            ),
                            l.a.createElement(
                              'div',
                              { className: 'level' },
                              l.a.createElement('em', null, 'Native speaker')
                            )
                          ),
                          l.a.createElement(
                            'div',
                            { className: 'item' },
                            l.a.createElement(
                              'div',
                              { className: 'language card-title' },
                              'Spanish'
                            ),
                            l.a.createElement(
                              'div',
                              { className: 'level' },
                              l.a.createElement('em', null, 'Conversational')
                            )
                          ),
                          l.a.createElement(
                            'div',
                            { className: 'item' },
                            l.a.createElement(
                              'div',
                              { className: 'language card-title' },
                              'German'
                            ),
                            l.a.createElement(
                              'div',
                              { className: 'level' },
                              l.a.createElement('em', null, 'Basic')
                            )
                          )
                        ),
                        l.a.createElement('hr', { className: 'col-9 hr' }),
                        l.a.createElement(
                          'section',
                          { className: 'col-10 flex-column', id: 'interests' },
                          l.a.createElement(
                            'h2',
                            {
                              className:
                                'card-title font-weight-bold resume-head',
                            },
                            'Interests'
                          ),
                          l.a.createElement(
                            'div',
                            { className: 'item' },
                            l.a.createElement(
                              'div',
                              { className: 'card-title font-weight-bold' },
                              'Photography & Digital Art'
                            ),
                            l.a.createElement(
                              'ul',
                              { className: 'keywords' },
                              l.a.createElement('li', null, 'Neon Noir'),
                              l.a.createElement('li', null, 'Cityscapes'),
                              l.a.createElement(
                                'li',
                                null,
                                'Unrecognized Beauty'
                              )
                            )
                          ),
                          l.a.createElement(
                            'div',
                            { className: 'item' },
                            l.a.createElement(
                              'div',
                              { className: 'card-title font-weight-bold' },
                              'Self Education'
                            ),
                            l.a.createElement(
                              'ul',
                              { className: 'keywords' },
                              l.a.createElement('li', null, 'Technology'),
                              l.a.createElement('li', null, 'Self Improvement'),
                              l.a.createElement('li', null, 'Self Led Learning')
                            )
                          )
                        ),
                        l.a.createElement(
                          'section',
                          { id: 'references', className: 'col-10 flex-column' },
                          l.a.createElement(
                            'h2',
                            {
                              className:
                                'card-title font-weight-bold resume-head',
                            },
                            'References'
                          ),
                          l.a.createElement(
                            'div',
                            { className: 'item' },
                            l.a.createElement(
                              'div',
                              { className: 'card-title font-weight-bold' },
                              'Kenneth Gary - Peer'
                            ),
                            l.a.createElement(
                              'blockquote',
                              null,
                              l.a.createElement(s.a, {
                                className: 'reference',
                                tel: '1-510-875-9086',
                              })
                            )
                          ),
                          l.a.createElement(
                            'div',
                            { className: 'item' },
                            l.a.createElement(
                              'div',
                              { className: 'card-title font-weight-bold' },
                              'Tim Weiland - Customer'
                            ),
                            l.a.createElement(
                              'blockquote',
                              { className: 'reference' },
                              l.a.createElement(s.a, {
                                className: 'reference',
                                tel: '1-650-773-4744',
                              })
                            )
                          ),
                          l.a.createElement(
                            'div',
                            { className: 'item last-item' },
                            l.a.createElement(
                              'div',
                              { className: 'card-title font-weight-bold' },
                              'Austin Blaylock - Employee'
                            ),
                            l.a.createElement(
                              'blockquote',
                              null,
                              l.a.createElement(s.a, {
                                className: 'reference',
                                tel: '1-925-337-2712',
                              })
                            )
                          )
                        ),
                        l.a.createElement('br', {
                          className: 'col-8 offset-2 d-block hr',
                        })
                      )
                    )
                  )
                )
              )
            );
          }),
          n
        );
      })(l.a.Component);
      t.default = u;
    },
    281: function(e, t, a) {
      'use strict';
      var n = a(1);
      (t.__esModule = !0),
        (t.default = function(e, t) {
          var a = void 0 === t ? {} : t,
            n = a.displayName,
            c = void 0 === n ? m(e) : n,
            u = a.Component,
            d = void 0 === u ? 'div' : u,
            f = a.defaultProps,
            h = s.default.forwardRef(function(t, a) {
              var n = t.className,
                c = t.bsPrefix,
                m = t.as,
                u = void 0 === m ? d : m,
                f = (0, r.default)(t, ['className', 'bsPrefix', 'as']),
                h = (0, o.useBootstrapPrefix)(c, e);
              return s.default.createElement(
                u,
                (0, l.default)({ ref: a, className: (0, i.default)(n, h) }, f)
              );
            });
          return (h.defaultProps = f), (h.displayName = c), h;
        });
      var l = n(a(28)),
        r = n(a(56)),
        i = n(a(26)),
        s = n(a(0)),
        c = n(a(98)),
        o = a(174),
        m = function(e) {
          return e[0].toUpperCase() + (0, c.default)(e).slice(1);
        };
      e.exports = t.default;
    },
    282: function(e, t, a) {
      'use strict';
      var n = a(1);
      (t.__esModule = !0), (t.default = void 0);
      var l = n(a(28)),
        r = n(a(0)),
        i = n(a(26));
      (t.default = function(e) {
        return r.default.forwardRef(function(t, a) {
          return r.default.createElement(
            'div',
            (0, l.default)({}, t, {
              ref: a,
              className: (0, i.default)(t.className, e),
            })
          );
        });
      }),
        (e.exports = t.default);
    },
    285: function(e, t, a) {
      'use strict';
      var n = a(62),
        l = a(1);
      (t.__esModule = !0), (t.default = void 0);
      var r = l(a(28)),
        i = l(a(56)),
        s = l(a(26)),
        c = n(a(0)),
        o = a(174),
        m = l(a(281)),
        u = l(a(282)),
        d = l(a(286)),
        f = l(a(287)),
        h = (0, u.default)('h5'),
        p = (0, u.default)('h6'),
        E = (0, m.default)('card-body'),
        b = c.default.forwardRef(function(e, t) {
          var a = e.bsPrefix,
            n = e.className,
            l = e.bg,
            m = e.text,
            u = e.border,
            f = e.body,
            h = e.children,
            p = e.as,
            b = void 0 === p ? 'div' : p,
            v = (0, i.default)(e, [
              'bsPrefix',
              'className',
              'bg',
              'text',
              'border',
              'body',
              'children',
              'as',
            ]),
            g = (0, o.useBootstrapPrefix)(a, 'card'),
            y = (0, c.useMemo)(
              function() {
                return { cardHeaderBsPrefix: g + '-header' };
              },
              [g]
            );
          return c.default.createElement(
            d.default.Provider,
            { value: y },
            c.default.createElement(
              b,
              (0, r.default)({ ref: t }, v, {
                className: (0, s.default)(
                  n,
                  g,
                  l && 'bg-' + l,
                  m && 'text-' + m,
                  u && 'border-' + u
                ),
              }),
              f ? c.default.createElement(E, null, h) : h
            )
          );
        });
      (b.displayName = 'Card'),
        (b.defaultProps = { body: !1 }),
        (b.Img = f.default),
        (b.Title = (0, m.default)('card-title', { Component: h })),
        (b.Subtitle = (0, m.default)('card-subtitle', { Component: p })),
        (b.Body = E),
        (b.Link = (0, m.default)('card-link', { Component: 'a' })),
        (b.Text = (0, m.default)('card-text', { Component: 'p' })),
        (b.Header = (0, m.default)('card-header')),
        (b.Footer = (0, m.default)('card-footer')),
        (b.ImgOverlay = (0, m.default)('card-img-overlay'));
      var v = b;
      (t.default = v), (e.exports = t.default);
    },
    286: function(e, t, a) {
      'use strict';
      var n = a(1);
      (t.__esModule = !0), (t.default = void 0);
      var l = n(a(0)).default.createContext(null);
      (t.default = l), (e.exports = t.default);
    },
    287: function(e, t, a) {
      'use strict';
      var n = a(1);
      (t.__esModule = !0), (t.default = void 0);
      var l = n(a(28)),
        r = n(a(56)),
        i = n(a(26)),
        s = n(a(0)),
        c = a(174),
        o = s.default.forwardRef(function(e, t) {
          var a = e.bsPrefix,
            n = e.className,
            o = e.variant,
            m = e.as,
            u = void 0 === m ? 'img' : m,
            d = (0, r.default)(e, ['bsPrefix', 'className', 'variant', 'as']),
            f = (0, c.useBootstrapPrefix)(a, 'card-img');
          return s.default.createElement(
            u,
            (0, l.default)(
              { ref: t, className: (0, i.default)(o ? f + '-' + o : f, n) },
              d
            )
          );
        });
      (o.displayName = 'CardImg'), (o.defaultProps = { variant: null });
      var m = o;
      (t.default = m), (e.exports = t.default);
    },
    328: function(e, t, a) {
      'use strict';
      a(40),
        a(42),
        a(18),
        a(27),
        a(29),
        a(124),
        a(57),
        a(19),
        a(21),
        a(63),
        a(41),
        a(5),
        a(6),
        a(2),
        a(9),
        a(36),
        Object.defineProperty(t, '__esModule', { value: !0 }),
        (t.default = void 0);
      var n = (function(e) {
        if (e && e.__esModule) return e;
        var t = {};
        if (null != e)
          for (var a in e)
            if (Object.prototype.hasOwnProperty.call(e, a)) {
              var n =
                Object.defineProperty && Object.getOwnPropertyDescriptor
                  ? Object.getOwnPropertyDescriptor(e, a)
                  : {};
              n.get || n.set ? Object.defineProperty(t, a, n) : (t[a] = e[a]);
            }
        return (t.default = e), t;
      })(a(0));
      function l(e) {
        for (var t = 1; t < arguments.length; t++) {
          var a = null != arguments[t] ? arguments[t] : {},
            n = Object.keys(a);
          'function' == typeof Object.getOwnPropertySymbols &&
            (n = n.concat(
              Object.getOwnPropertySymbols(a).filter(function(e) {
                return Object.getOwnPropertyDescriptor(a, e).enumerable;
              })
            )),
            n.forEach(function(t) {
              r(e, t, a[t]);
            });
        }
        return e;
      }
      function r(e, t, a) {
        return (
          t in e
            ? Object.defineProperty(e, t, {
                value: a,
                enumerable: !0,
                configurable: !0,
                writable: !0,
              })
            : (e[t] = a),
          e
        );
      }
      function i(e, t) {
        if (null == e) return {};
        var a,
          n,
          l = (function(e, t) {
            if (null == e) return {};
            var a,
              n,
              l = {},
              r = Object.keys(e);
            for (n = 0; n < r.length; n++)
              (a = r[n]), t.indexOf(a) >= 0 || (l[a] = e[a]);
            return l;
          })(e, t);
        if (Object.getOwnPropertySymbols) {
          var r = Object.getOwnPropertySymbols(e);
          for (n = 0; n < r.length; n++)
            (a = r[n]),
              t.indexOf(a) >= 0 ||
                (Object.prototype.propertyIsEnumerable.call(e, a) &&
                  (l[a] = e[a]));
        }
        return l;
      }
      function s(e) {
        return (s =
          'function' == typeof Symbol && 'symbol' == typeof Symbol.iterator
            ? function(e) {
                return typeof e;
              }
            : function(e) {
                return e &&
                  'function' == typeof Symbol &&
                  e.constructor === Symbol &&
                  e !== Symbol.prototype
                  ? 'symbol'
                  : typeof e;
              })(e);
      }
      function c(e, t) {
        for (var a = 0; a < t.length; a++) {
          var n = t[a];
          (n.enumerable = n.enumerable || !1),
            (n.configurable = !0),
            'value' in n && (n.writable = !0),
            Object.defineProperty(e, n.key, n);
        }
      }
      function o(e, t) {
        return !t || ('object' !== s(t) && 'function' != typeof t)
          ? (function(e) {
              if (void 0 === e)
                throw new ReferenceError(
                  "this hasn't been initialised - super() hasn't been called"
                );
              return e;
            })(e)
          : t;
      }
      function m(e) {
        return (m = Object.setPrototypeOf
          ? Object.getPrototypeOf
          : function(e) {
              return e.__proto__ || Object.getPrototypeOf(e);
            })(e);
      }
      function u(e, t) {
        return (u =
          Object.setPrototypeOf ||
          function(e, t) {
            return (e.__proto__ = t), e;
          })(e, t);
      }
      var d = (function(e) {
        function t(e) {
          var a;
          return (
            (function(e, t) {
              if (!(e instanceof t))
                throw new TypeError('Cannot call a class as a function');
            })(this, t),
            ((a = o(this, m(t).call(this, e))).state = {
              humanInteraction: !1,
            }),
            a
          );
        }
        var a, r, d;
        return (
          (function(e, t) {
            if ('function' != typeof t && null !== t)
              throw new TypeError(
                'Super expression must either be null or a function'
              );
            (e.prototype = Object.create(t && t.prototype, {
              constructor: { value: e, writable: !0, configurable: !0 },
            })),
              t && u(e, t);
          })(t, n.Component),
          (a = t),
          (r = [
            {
              key: 'createContactLink',
              value: function(e) {
                var t, a;
                if (e.email)
                  (t = 'mailto:'.concat(e.email)),
                    e.headers &&
                      (t += '?'.concat(
                        ((a = e.headers),
                        Object.keys(a)
                          .map(function(e) {
                            return ''
                              .concat(e, '=')
                              .concat(encodeURIComponent(a[e]));
                          })
                          .join('&'))
                      ));
                else if (e.tel) t = 'tel:'.concat(e.tel);
                else if (e.sms) t = 'sms:'.concat(e.sms);
                else if (e.facetime) t = 'facetime:'.concat(e.facetime);
                else if (e.href) t = e.href;
                else {
                  if ('object' === s(e.children)) return '';
                  t = e.children;
                }
                return t;
              },
            },
            {
              key: 'handleClick',
              value: function(e) {
                var t = this.props.onClick;
                !1 === this.state.humanInteraction &&
                  (e.preventDefault(),
                  t && 'function' == typeof t && t(),
                  (window.location.href = this.createContactLink(this.props)));
              },
            },
            {
              key: 'handleCopiability',
              value: function() {
                this.setState({ humanInteraction: !0 });
              },
            },
            {
              key: 'reverse',
              value: function(e) {
                if (void 0 !== e)
                  return e
                    .split('')
                    .reverse()
                    .join('')
                    .replace('(', ')')
                    .replace(')', '(');
              },
            },
            {
              key: 'render',
              value: function() {
                var e = this.state.humanInteraction,
                  t = this.props,
                  a = t.element,
                  r = void 0 === a ? 'a' : a,
                  c = t.children,
                  o = t.tel,
                  m = t.sms,
                  u = t.facetime,
                  d = t.email,
                  f = t.href,
                  h = (t.headers, t.obfuscate),
                  p = t.obfuscateChildren,
                  E = t.linkText,
                  b = t.style,
                  v = i(t, [
                    'element',
                    'children',
                    'tel',
                    'sms',
                    'facetime',
                    'email',
                    'href',
                    'headers',
                    'obfuscate',
                    'obfuscateChildren',
                    'linkText',
                    'style',
                  ]),
                  g = c || o || m || u || d || f,
                  y = l({}, b || {}, {
                    unicodeBidi: 'bidi-override',
                    direction: !0 === e || !1 === h || !1 === p ? 'ltr' : 'rtl',
                  }),
                  N =
                    !0 === e || !1 === h || 'object' === s(c) || !1 === p
                      ? g
                      : this.reverse(g),
                  w =
                    'a' === r
                      ? {
                          href:
                            !0 === e || !1 === h
                              ? this.createContactLink(this.props)
                              : E || 'obfuscated',
                          onClick: this.handleClick.bind(this),
                        }
                      : {},
                  k = l(
                    {
                      onFocus: this.handleCopiability.bind(this),
                      onMouseOver: this.handleCopiability.bind(this),
                      onContextMenu: this.handleCopiability.bind(this),
                    },
                    v,
                    w,
                    { style: y }
                  );
                return n.default.createElement(r, k, N);
              },
            },
          ]) && c(a.prototype, r),
          d && c(a, d),
          t
        );
      })();
      t.default = d;
    },
    329: function(e, t, a) {
      'use strict';
      (function(e) {
        a(124),
          a(17),
          Object.defineProperty(t, '__esModule', { value: !0 }),
          (t.componentTransitionTime = t.pageTransitionExists = t.pageTransitionTime = t.pageTransitionEvent = void 0);
        var n =
            Object.assign ||
            function(e) {
              for (var t = 1; t < arguments.length; t++) {
                var a = arguments[t];
                for (var n in a)
                  Object.prototype.hasOwnProperty.call(a, n) && (e[n] = a[n]);
              }
              return e;
            },
          l = (function() {
            function e(e, t) {
              for (var a = 0; a < t.length; a++) {
                var n = t[a];
                (n.enumerable = n.enumerable || !1),
                  (n.configurable = !0),
                  'value' in n && (n.writable = !0),
                  Object.defineProperty(e, n.key, n);
              }
            }
            return function(t, a, n) {
              return a && e(t.prototype, a), n && e(t, n), t;
            };
          })(),
          r = c(a(0)),
          i = c(a(23)),
          s = c(a(125));
        function c(e) {
          return e && e.__esModule ? e : { default: e };
        }
        var o = (t.pageTransitionEvent = 'gatsby-plugin-page-transition::exit'),
          m = (t.pageTransitionTime = 'gatsby-plugin-page-transition::time'),
          u = (t.pageTransitionExists =
            'gatsby-plugin-page-transition::exists'),
          d = (t.componentTransitionTime =
            'gatsby-plugin-page-transition::comTime'),
          f = (function(t) {
            function a(e) {
              !(function(e, t) {
                if (!(e instanceof t))
                  throw new TypeError('Cannot call a class as a function');
              })(this, a);
              var t = (function(e, t) {
                if (!e)
                  throw new ReferenceError(
                    "this hasn't been initialised - super() hasn't been called"
                  );
                return !t || ('object' != typeof t && 'function' != typeof t)
                  ? e
                  : t;
              })(this, (a.__proto__ || Object.getPrototypeOf(a)).call(this, e));
              return (
                (t.setIn = t.setIn.bind(t)),
                (t.listenerHandler = t.listenerHandler.bind(t)),
                (t.state = { in: !1, transitionTime: 0 }),
                t
              );
            }
            return (
              (function(e, t) {
                if ('function' != typeof t && null !== t)
                  throw new TypeError(
                    'Super expression must either be null or a function, not ' +
                      typeof t
                  );
                (e.prototype = Object.create(t && t.prototype, {
                  constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0,
                  },
                })),
                  t &&
                    (Object.setPrototypeOf
                      ? Object.setPrototypeOf(e, t)
                      : (e.__proto__ = t));
              })(a, r.default.Component),
              l(a, [
                {
                  key: 'componentDidMount',
                  value: function() {
                    this.setIn(!0),
                      e.window.addEventListener(o, this.listenerHandler),
                      (e.window[u] = !0),
                      (e.window[d] = this.props.transitionTime),
                      this.setState({
                        transitionTime:
                          this.props.transitionTime || e.window[m],
                      });
                  },
                },
                {
                  key: 'componentWillUnmount',
                  value: function() {
                    e.window.removeEventListener(o, this.listenerHandler),
                      (e.window[u] = !1),
                      (e.window[d] = void 0);
                  },
                },
                {
                  key: 'setIn',
                  value: function(e) {
                    this.setState({ in: e });
                  },
                },
                {
                  key: 'listenerHandler',
                  value: function(t) {
                    e.window.location.pathname !== t.detail.pathname &&
                      this.setIn(!1);
                  },
                },
                {
                  key: 'render',
                  value: function() {
                    var e = this,
                      t = this.props.defaultStyle || {
                        transition:
                          'opacity ' +
                          this.state.transitionTime +
                          'ms ease-in-out',
                        opacity: 0,
                      },
                      a = this.props.transitionStyles || {
                        entering: { opacity: 1 },
                        entered: { opacity: 1 },
                        exiting: { opacity: 0 },
                      };
                    return r.default.createElement(
                      s.default,
                      { in: this.state.in, timeout: this.state.transitionTime },
                      function(l) {
                        return r.default.createElement(
                          'div',
                          { style: n({}, t, a[l]) },
                          e.props.children
                        );
                      }
                    );
                  },
                },
              ]),
              a
            );
          })();
        (f.propTypes = {
          children: i.default.oneOfType([
            i.default.arrayOf(i.default.shape({})),
            i.default.shape({}),
          ]).isRequired,
          transitionTime: i.default.number,
          defaultStyle: i.default.shape({}),
          transitionStyles: i.default.shape({}),
        }),
          (t.default = f);
      }.call(this, a(123)));
    },
  },
]);
//# sourceMappingURL=component---src-pages-resume-jsx-a9cce0c3d9dd24aba02e.js.map
