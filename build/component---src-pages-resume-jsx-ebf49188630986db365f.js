(window.webpackJsonp = window.webpackJsonp || []).push([
  [8],
  {
    284: function(e, t, a) {
      'use strict';
      a.r(t);
      var l = a(0),
        n = a.n(l),
        r = a(277),
        s = a(339),
        c = a.n(s),
        i = (a(177), a(278)),
        o = a(296),
        m = a.n(o);
      var u = (function(e) {
        var t, a;
        function l() {
          return e.apply(this, arguments) || this;
        }
        return (
          (a = e),
          ((t = l).prototype = Object.create(a.prototype)),
          (t.prototype.constructor = t),
          (t.__proto__ = a),
          (l.prototype.render = function() {
            return n.a.createElement(
              r.a,
              null,
              n.a.createElement(i.a, {
                title: '[resume]',
                description:
                  'created from the output of JSONresume into HTML adapted into JSX, this is the live resume of TLH',
              }),
              n.a.createElement(
                m.a,
                null,
                n.a.createElement(
                  'div',
                  { id: 'resume' },
                  n.a.createElement(
                    'div',
                    { className: 'card-resume' },
                    n.a.createElement(
                      'section',
                      null,
                      n.a.createElement(
                        'div',
                        { className: 'resume-head col-10 mt-5 offset-1' },
                        n.a.createElement(
                          'h1',
                          { className: 'card-title font-weight-bold col-9' },
                          'Thomas Leon Highbaugh'
                        ),
                        n.a.createElement(
                          'h2',
                          { className: 'card-subtitle ml-4 col-11' },
                          'Software Engineer & Freelance IT Professional'
                        )
                      ),
                      n.a.createElement(
                        'div',
                        { id: 'content', className: 'card-body offset-2' },
                        n.a.createElement(
                          'section',
                          { id: 'basics' },
                          n.a.createElement(
                            'div',
                            { className: 'contact card-title' },
                            n.a.createElement(
                              'h2',
                              { className: 'card-subtitle' },
                              'Contact'
                            ),
                            n.a.createElement(
                              'div',
                              { className: 'contact' },
                              n.a.createElement('strong', null, 'Website: '),
                              n.a.createElement(
                                'a',
                                { href: 'http://thomasleonhighbaugh.me' },
                                'thomasleonhighbaugh.me'
                              )
                            ),
                            n.a.createElement(
                              'div',
                              { className: 'contact' },
                              n.a.createElement('strong', null, 'Email: '),
                              n.a.createElement(c.a, {
                                email: 'thighbaugh@zoho.com',
                              })
                            ),
                            n.a.createElement(
                              'div',
                              { className: 'contact' },
                              n.a.createElement('strong', null, 'Phone: '),
                              n.a.createElement(c.a, { tel: '510-907-0654' })
                            )
                          ),
                          n.a.createElement(
                            'div',
                            { className: 'summary' },
                            n.a.createElement('h3', null, 'About'),
                            n.a.createElement(
                              'p',
                              null,
                              'A self-taught software developer with an eye for design and a passion for Linux.'
                            )
                          )
                        ),
                        n.a.createElement(
                          'section',
                          { className: 'btn-group', id: 'profiles' },
                          n.a.createElement(
                            'h2',
                            { className: 'card-subtitle' },
                            'Internet Profiles & Personal Sites'
                          ),
                          n.a.createElement(
                            'a',
                            {
                              className: 'nav-btn',
                              href:
                                'http://linkedin.com/in/thomas-leon-highbaugh',
                            },
                            'LinkedIn'
                          ),
                          n.a.createElement(
                            'a',
                            {
                              className: 'nav-btn',
                              href: 'https://github.com/Thomashighbaugh',
                            },
                            'GitHub'
                          ),
                          n.a.createElement(
                            'a',
                            {
                              className: 'nav-btn',
                              href: 'https://dribbble.com/thighbaugh',
                            },
                            'Dribbble'
                          ),
                          n.a.createElement(
                            'a',
                            {
                              className: 'nav-btn',
                              href: 'https://gitlab.com/thighbaugh',
                            },
                            'GitLab'
                          ),
                          n.a.createElement(
                            'a',
                            {
                              className: 'nav-btn',
                              href: 'https://instagram.com/tlh-resurgens',
                            },
                            'Instagram'
                          ),
                          n.a.createElement(
                            'a',
                            {
                              className: 'nav-btn',
                              href: 'https://gitlab.com/thighbaugh',
                            },
                            'Blog                  '
                          ),
                          n.a.createElement(
                            'a',
                            {
                              className: 'nav-btn',
                              href: 'https://gitlab.com/thighbaugh',
                            },
                            'Gallery                  '
                          )
                        ),
                        n.a.createElement(
                          'section',
                          { className: 'col-10', id: 'work' },
                          n.a.createElement(
                            'h2',
                            {
                              className:
                                'card-title font-weight-bold resume-head',
                            },
                            'Work'
                          ),
                          n.a.createElement(
                            'div',
                            { className: 'item' },
                            n.a.createElement(
                              'h3',
                              { className: 'work_name' },
                              'Freelance'
                            ),
                            n.a.createElement(
                              'div',
                              { className: 'work_position' },
                              'Web Developer'
                            ),
                            n.a.createElement(
                              'div',
                              { className: 'work_date' },
                              n.a.createElement(
                                'span',
                                { className: 'startDate' },
                                '2018-03-15'
                              ),
                              n.a.createElement(
                                'span',
                                { className: 'endDate' },
                                '- present'
                              )
                            ),
                            n.a.createElement(
                              'div',
                              { className: 'work_website' },
                              n.a.createElement(
                                'a',
                                {
                                  className: 'nav-btn',
                                  href: 'https://thomasleonhighbaugh.me',
                                },
                                'https://thomasleonhighbaugh.me'
                              )
                            ),
                            n.a.createElement(
                              'ul',
                              { className: 'highlights' },
                              n.a.createElement(
                                'li',
                                null,
                                'Designing and coding websites for customers from the ground up'
                              ),
                              n.a.createElement(
                                'li',
                                null,
                                'Developing customer relationships and responding to customer inquiries'
                              ),
                              n.a.createElement(
                                'li',
                                null,
                                'Keeping ahead of industry trends while continuing education about web-related technologies'
                              ),
                              n.a.createElement(
                                'li',
                                null,
                                'Vigilant market research and analysis of the competition'
                              )
                            )
                          ),
                          n.a.createElement('hr', null),
                          n.a.createElement(
                            'div',
                            { className: 'item' },
                            n.a.createElement(
                              'h3',
                              { className: 'work_name' },
                              'Freelance'
                            ),
                            n.a.createElement(
                              'div',
                              { className: 'work_position' },
                              'Computer Repair Technician'
                            ),
                            n.a.createElement(
                              'div',
                              { className: 'work_date' },
                              n.a.createElement(
                                'span',
                                { className: 'startDate' },
                                '2018-02-18'
                              ),
                              n.a.createElement(
                                'span',
                                { className: 'endDate' },
                                '- present'
                              )
                            ),
                            n.a.createElement(
                              'div',
                              { className: 'work_website' },
                              n.a.createElement(
                                'a',
                                {
                                  className: 'nav-btn',
                                  href: 'https://thomasleonhighbaugh.me',
                                },
                                'https://thomasleonhighbaugh.me'
                              )
                            ),
                            n.a.createElement(
                              'ul',
                              { className: 'highlights' },
                              n.a.createElement(
                                'li',
                                null,
                                'Troubleshooting PCs that are not performing as expected'
                              ),
                              n.a.createElement(
                                'li',
                                null,
                                'Educating customers about routine PC maintanence and security'
                              ),
                              n.a.createElement(
                                'li',
                                null,
                                'Designing and implementing PC builds based on customer budgets and desired use cases'
                              ),
                              n.a.createElement(
                                'li',
                                null,
                                'Marketing and customer relationship development to maintain stream of work'
                              ),
                              n.a.createElement(
                                'li',
                                null,
                                'Learning and keeping tabs on personal and commercial electronic markets'
                              ),
                              n.a.createElement(
                                'li',
                                null,
                                'Market research to keep abilities in sync with customer needs'
                              )
                            )
                          ),
                          n.a.createElement('hr', null),
                          n.a.createElement(
                            'div',
                            { className: 'item' },
                            n.a.createElement(
                              'h3',
                              { className: 'work_name' },
                              'Pet Food Express: Blackhawk'
                            ),
                            n.a.createElement(
                              'div',
                              { className: 'work_position' },
                              'Assistant Manager IV'
                            ),
                            n.a.createElement(
                              'div',
                              { className: 'work_date' },
                              n.a.createElement(
                                'span',
                                { className: 'startDate' },
                                '2012-05-13'
                              ),
                              n.a.createElement(
                                'span',
                                { className: 'endDate' },
                                '- 2018-04-19'
                              )
                            ),
                            n.a.createElement(
                              'div',
                              { className: 'work_website' },
                              n.a.createElement(
                                'a',
                                {
                                  className: 'nav-btn',
                                  href: 'https://petfoodexpress.com',
                                },
                                'https://petfoodexpress.com'
                              )
                            ),
                            n.a.createElement(
                              'ul',
                              { className: 'highlights' },
                              n.a.createElement(
                                'li',
                                null,
                                'Educating customers about holistic pet foods and how to appropriately use them'
                              ),
                              n.a.createElement(
                                'li',
                                null,
                                'Staff development and consultation to ensure corporate standards are complied with'
                              ),
                              n.a.createElement(
                                'li',
                                null,
                                'Setting mood of the staff and maintaining a customer-first atmosphere'
                              ),
                              n.a.createElement(
                                'li',
                                null,
                                'Assisting store manager in daily cash and store operations'
                              ),
                              n.a.createElement(
                                'li',
                                null,
                                'Ensuring store is tidy and stocked at all times'
                              ),
                              n.a.createElement(
                                'li',
                                null,
                                'Managing customer comments and concerns with sensitivity and promptness'
                              )
                            )
                          )
                        ),
                        n.a.createElement('hr', {
                          className: 'hr offset-2 col-8',
                        }),
                        n.a.createElement(
                          'section',
                          { className: 'col-10', id: 'volunteer' },
                          n.a.createElement(
                            'h2',
                            {
                              className:
                                'card-title font-weight-bold resume-head',
                            },
                            'Volunteer'
                          ),
                          n.a.createElement(
                            'div',
                            { className: 'item' },
                            n.a.createElement(
                              'h3',
                              { className: 'company' },
                              'South Hayward Parish'
                            ),
                            n.a.createElement(
                              'div',
                              { className: 'work_position' },
                              'Volunteer'
                            ),
                            n.a.createElement(
                              'div',
                              { className: 'summary' },
                              n.a.createElement(
                                'p',
                                null,
                                'A small homeless shelter in Hayward, California that leverages volunteer work and community donations to help the needy'
                              )
                            ),
                            n.a.createElement(
                              'ul',
                              { className: 'highlights' },
                              n.a.createElement(
                                'li',
                                null,
                                'Assisted in rehabilitating old PCs for resident use in job seeking'
                              ),
                              n.a.createElement(
                                'li',
                                null,
                                'Stocking kitchen with fresh donations from local grocery stores'
                              ),
                              n.a.createElement(
                                'li',
                                null,
                                'Assisting residents in using PCs to find jobs'
                              ),
                              n.a.createElement(
                                'li',
                                null,
                                'Talking to residents and relaying positive messages to keep them motivated'
                              )
                            )
                          )
                        ),
                        n.a.createElement('hr', {
                          className: 'hr offset-2 col-8',
                        }),
                        n.a.createElement(
                          'section',
                          { className: 'col-10', id: 'education' },
                          n.a.createElement(
                            'h2',
                            {
                              className:
                                'card-title font-weight-bold resume-head',
                            },
                            'Education'
                          ),
                          n.a.createElement(
                            'div',
                            { className: 'item' },
                            n.a.createElement(
                              'div',
                              { className: 'institution' },
                              'Codify Academy'
                            ),
                            n.a.createElement(
                              'div',
                              { className: 'study_date' },
                              n.a.createElement(
                                'span',
                                { className: 'startDate' },
                                '2018-02-14'
                              ),
                              n.a.createElement(
                                'span',
                                { className: 'endDate' },
                                '- 2018-05-30'
                              )
                            ),
                            n.a.createElement(
                              'ul',
                              { className: 'courses' },
                              n.a.createElement('li', null, 'HTML5'),
                              n.a.createElement('li', null, 'CSS3'),
                              n.a.createElement('li', null, 'Javascript'),
                              n.a.createElement('li', null, 'Vue'),
                              n.a.createElement('li', null, 'Restful APIs'),
                              n.a.createElement('li', null, 'Front End Design'),
                              n.a.createElement('li', null, 'Git'),
                              n.a.createElement(
                                'li',
                                null,
                                'Web Development Workflows'
                              )
                            ),
                            n.a.createElement(
                              'div',
                              { className: 'gpa' },
                              n.a.createElement('span', null, 'GPA 4.0')
                            )
                          ),
                          n.a.createElement('hr', null),
                          n.a.createElement(
                            'div',
                            { className: 'item' },
                            n.a.createElement(
                              'div',
                              { className: 'institution' },
                              'Las Positas/Chabot Community College'
                            ),
                            n.a.createElement(
                              'div',
                              { className: 'study_date' },
                              n.a.createElement(
                                'span',
                                { className: 'startDate' },
                                '2018-02-14'
                              ),
                              n.a.createElement(
                                'span',
                                { className: 'endDate' },
                                '- 2018-05-30'
                              )
                            ),
                            n.a.createElement(
                              'ul',
                              { className: 'courses' },
                              n.a.createElement('li', null, 'HTML5/CSS3'),
                              n.a.createElement('li', null, 'Linux'),
                              n.a.createElement('li', null, 'C++'),
                              n.a.createElement('li', null, 'Java'),
                              n.a.createElement('li', null, 'Javascript'),
                              n.a.createElement('li', null, 'Ethical Hacking')
                            ),
                            n.a.createElement(
                              'div',
                              { className: 'gpa' },
                              n.a.createElement('span', null, ' GPA 4.0')
                            )
                          ),
                          n.a.createElement('hr', null),
                          n.a.createElement(
                            'div',
                            { className: 'item' },
                            n.a.createElement(
                              'div',
                              { className: 'institution' },
                              'California State University East Bay (unrelated major)'
                            ),
                            n.a.createElement(
                              'div',
                              { className: 'study_date' },
                              n.a.createElement(
                                'span',
                                { className: 'startDate' },
                                '2009-09'
                              ),
                              n.a.createElement(
                                'span',
                                { className: 'endDate' },
                                '- 2014-04'
                              )
                            ),
                            n.a.createElement(
                              'ul',
                              { className: 'courses' },
                              n.a.createElement('li', null, 'C++'),
                              n.a.createElement('li', null, 'Linux'),
                              n.a.createElement('li', null, 'Java'),
                              n.a.createElement('li', null, 'Logic'),
                              n.a.createElement('li', null, 'Economics (Macro)')
                            ),
                            n.a.createElement(
                              'div',
                              { className: 'gpa' },
                              n.a.createElement('span', null, ' GPA 3.7')
                            )
                          )
                        ),
                        n.a.createElement('hr', {
                          className: 'hr offset-2 col-8',
                        }),
                        n.a.createElement(
                          'section',
                          { id: 'skills', className: 'flex-column col-10' },
                          n.a.createElement(
                            'h2',
                            {
                              className:
                                'card-title font-weight-bold resume-head',
                            },
                            'Skills'
                          ),
                          n.a.createElement(
                            'div',
                            { className: 'item' },
                            n.a.createElement(
                              'div',
                              { className: 'card-title font-weight-bold' },
                              'Web Development'
                            ),
                            n.a.createElement(
                              'ul',
                              { className: 'keywords' },
                              n.a.createElement('li', null, 'HTML5 & CSS3'),
                              n.a.createElement(
                                'li',
                                null,
                                'Javascript, JSX, Node & React'
                              ),
                              n.a.createElement(
                                'li',
                                null,
                                'PHP, LAMP & MySQL'
                              ),
                              n.a.createElement('li', null, 'Python & Perl'),
                              n.a.createElement(
                                'li',
                                null,
                                'SASS, SCSS & Less'
                              ),
                              n.a.createElement(
                                'li',
                                null,
                                'BASH, ZSH & Shell Scripting'
                              )
                            )
                          ),
                          n.a.createElement(
                            'div',
                            { className: 'item' },
                            n.a.createElement(
                              'div',
                              { className: 'card-title font-weight-bold' },
                              'Linux Systems Administration'
                            ),
                            n.a.createElement(
                              'ul',
                              { className: 'keywords' },
                              n.a.createElement('li', null, 'BASH'),
                              n.a.createElement('li', null, 'ZSH'),
                              n.a.createElement('li', null, 'Linux'),
                              n.a.createElement('li', null, 'Ansible'),
                              n.a.createElement('li', null, 'SSH'),
                              n.a.createElement('li', null, 'Virtualization'),
                              n.a.createElement('li', null, 'Containers')
                            )
                          ),
                          n.a.createElement(
                            'div',
                            { className: 'item' },
                            n.a.createElement(
                              'div',
                              { className: 'card-title font-weight-bold' },
                              'Hardware Configuration & Repair'
                            ),
                            n.a.createElement(
                              'ul',
                              { className: 'keywords' },
                              n.a.createElement('li', null, 'Windows'),
                              n.a.createElement('li', null, 'macOS'),
                              n.a.createElement('li', null, 'Linux'),
                              n.a.createElement('li', null, 'Server'),
                              n.a.createElement(
                                'li',
                                null,
                                'Component Installation'
                              ),
                              n.a.createElement('li', null, 'Troubleshooting')
                            )
                          ),
                          ' ',
                          n.a.createElement(
                            'div',
                            { className: 'item' },
                            n.a.createElement(
                              'div',
                              { className: 'card-title font-weight-bold' },
                              ' ',
                              'Linux Development',
                              ' '
                            ),
                            n.a.createElement(
                              'ul',
                              { className: 'keywords' },
                              n.a.createElement(
                                'li',
                                null,
                                'BASH Shell Scripting'
                              ),
                              n.a.createElement(
                                'li',
                                null,
                                'Electron Platform'
                              ),
                              n.a.createElement(
                                'li',
                                null,
                                'Linux Configuration (aka dotfiles)'
                              ),
                              n.a.createElement('li', null, 'C++')
                            )
                          )
                        ),
                        n.a.createElement('hr', {
                          className: 'col-8 offset-2 d-block hr',
                        }),
                        n.a.createElement(
                          'section',
                          { id: 'languages', className: 'flex-column col-10' },
                          n.a.createElement(
                            'h2',
                            {
                              className:
                                'card-title font-weight-bold resume-head',
                            },
                            'Languages'
                          ),
                          n.a.createElement(
                            'div',
                            { className: 'item' },
                            n.a.createElement(
                              'div',
                              { className: 'language card-title' },
                              'English'
                            ),
                            n.a.createElement(
                              'div',
                              { className: 'level' },
                              n.a.createElement('em', null, 'Native speaker')
                            )
                          ),
                          n.a.createElement(
                            'div',
                            { className: 'item' },
                            n.a.createElement(
                              'div',
                              { className: 'language card-title' },
                              'Spanish'
                            ),
                            n.a.createElement(
                              'div',
                              { className: 'level' },
                              n.a.createElement('em', null, 'Conversational')
                            )
                          ),
                          n.a.createElement(
                            'div',
                            { className: 'item' },
                            n.a.createElement(
                              'div',
                              { className: 'language card-title' },
                              'German'
                            ),
                            n.a.createElement(
                              'div',
                              { className: 'level' },
                              n.a.createElement('em', null, 'Basic')
                            )
                          )
                        ),
                        n.a.createElement('hr', { className: 'col-9 hr' }),
                        n.a.createElement(
                          'section',
                          { className: 'col-10 flex-column', id: 'interests' },
                          n.a.createElement(
                            'h2',
                            {
                              className:
                                'card-title font-weight-bold resume-head',
                            },
                            'Interests'
                          ),
                          n.a.createElement(
                            'div',
                            { className: 'item' },
                            n.a.createElement(
                              'div',
                              { className: 'card-title font-weight-bold' },
                              'Photography & Digital Art'
                            ),
                            n.a.createElement(
                              'ul',
                              { className: 'keywords' },
                              n.a.createElement('li', null, 'Neon Noir'),
                              n.a.createElement('li', null, 'Cityscapes'),
                              n.a.createElement(
                                'li',
                                null,
                                'Unrecognized Beauty'
                              )
                            )
                          ),
                          n.a.createElement(
                            'div',
                            { className: 'item' },
                            n.a.createElement(
                              'div',
                              { className: 'card-title font-weight-bold' },
                              'Self Education'
                            ),
                            n.a.createElement(
                              'ul',
                              { className: 'keywords' },
                              n.a.createElement('li', null, 'Technology'),
                              n.a.createElement('li', null, 'Self Improvement'),
                              n.a.createElement('li', null, 'Self Led Learning')
                            )
                          )
                        ),
                        n.a.createElement(
                          'section',
                          { id: 'references', className: 'col-10 flex-column' },
                          n.a.createElement(
                            'h2',
                            {
                              className:
                                'card-title font-weight-bold resume-head',
                            },
                            'References'
                          ),
                          n.a.createElement(
                            'div',
                            { className: 'item' },
                            n.a.createElement(
                              'div',
                              { className: 'card-title font-weight-bold' },
                              'Kenneth Gary - Peer'
                            ),
                            n.a.createElement(
                              'blockquote',
                              null,
                              n.a.createElement(c.a, {
                                className: 'reference',
                                tel: '1-510-875-9086',
                              })
                            )
                          ),
                          n.a.createElement(
                            'div',
                            { className: 'item' },
                            n.a.createElement(
                              'div',
                              { className: 'card-title font-weight-bold' },
                              'Tim Weiland - Customer'
                            ),
                            n.a.createElement(
                              'blockquote',
                              { className: 'reference' },
                              n.a.createElement(c.a, {
                                className: 'reference',
                                tel: '1-650-773-4744',
                              })
                            )
                          ),
                          n.a.createElement(
                            'div',
                            { className: 'item last-item' },
                            n.a.createElement(
                              'div',
                              { className: 'card-title font-weight-bold' },
                              'Austin Blaylock - Employee'
                            ),
                            n.a.createElement(
                              'blockquote',
                              null,
                              n.a.createElement(c.a, {
                                className: 'reference',
                                tel: '1-925-337-2712',
                              })
                            )
                          )
                        ),
                        n.a.createElement('br', {
                          className: 'col-8 offset-2 d-block hr',
                        })
                      )
                    )
                  )
                )
              )
            );
          }),
          l
        );
      })(n.a.Component);
      t.default = u;
    },
    292: function(e, t, a) {
      'use strict';
      var l = a(3);
      (t.__esModule = !0),
        (t.default = function(e, t) {
          var a = void 0 === t ? {} : t,
            l = a.displayName,
            i = void 0 === l ? m(e) : l,
            u = a.Component,
            d = void 0 === u ? 'div' : u,
            f = a.defaultProps,
            h = c.default.forwardRef(function(t, a) {
              var l = t.className,
                i = t.bsPrefix,
                m = t.as,
                u = void 0 === m ? d : m,
                f = (0, r.default)(t, ['className', 'bsPrefix', 'as']),
                h = (0, o.useBootstrapPrefix)(i, e);
              return c.default.createElement(
                u,
                (0, n.default)({ ref: a, className: (0, s.default)(l, h) }, f)
              );
            });
          return (h.defaultProps = f), (h.displayName = i), h;
        });
      var n = l(a(29)),
        r = l(a(57)),
        s = l(a(27)),
        c = l(a(0)),
        i = l(a(99)),
        o = a(174),
        m = function(e) {
          return e[0].toUpperCase() + (0, i.default)(e).slice(1);
        };
      e.exports = t.default;
    },
    293: function(e, t, a) {
      'use strict';
      var l = a(3);
      (t.__esModule = !0), (t.default = void 0);
      var n = l(a(29)),
        r = l(a(0)),
        s = l(a(27));
      (t.default = function(e) {
        return r.default.forwardRef(function(t, a) {
          return r.default.createElement(
            'div',
            (0, n.default)({}, t, {
              ref: a,
              className: (0, s.default)(t.className, e),
            })
          );
        });
      }),
        (e.exports = t.default);
    },
    296: function(e, t, a) {
      'use strict';
      var l = a(64),
        n = a(3);
      (t.__esModule = !0), (t.default = void 0);
      var r = n(a(29)),
        s = n(a(57)),
        c = n(a(27)),
        i = l(a(0)),
        o = a(174),
        m = n(a(292)),
        u = n(a(293)),
        d = n(a(297)),
        f = n(a(298)),
        h = (0, u.default)('h5'),
        E = (0, u.default)('h6'),
        p = (0, m.default)('card-body'),
        v = i.default.forwardRef(function(e, t) {
          var a = e.bsPrefix,
            l = e.className,
            n = e.bg,
            m = e.text,
            u = e.border,
            f = e.body,
            h = e.children,
            E = e.as,
            v = void 0 === E ? 'div' : E,
            b = (0, s.default)(e, [
              'bsPrefix',
              'className',
              'bg',
              'text',
              'border',
              'body',
              'children',
              'as',
            ]),
            g = (0, o.useBootstrapPrefix)(a, 'card'),
            N = (0, i.useMemo)(
              function() {
                return { cardHeaderBsPrefix: g + '-header' };
              },
              [g]
            );
          return i.default.createElement(
            d.default.Provider,
            { value: N },
            i.default.createElement(
              v,
              (0, r.default)({ ref: t }, b, {
                className: (0, c.default)(
                  l,
                  g,
                  n && 'bg-' + n,
                  m && 'text-' + m,
                  u && 'border-' + u
                ),
              }),
              f ? i.default.createElement(p, null, h) : h
            )
          );
        });
      (v.displayName = 'Card'),
        (v.defaultProps = { body: !1 }),
        (v.Img = f.default),
        (v.Title = (0, m.default)('card-title', { Component: h })),
        (v.Subtitle = (0, m.default)('card-subtitle', { Component: E })),
        (v.Body = p),
        (v.Link = (0, m.default)('card-link', { Component: 'a' })),
        (v.Text = (0, m.default)('card-text', { Component: 'p' })),
        (v.Header = (0, m.default)('card-header')),
        (v.Footer = (0, m.default)('card-footer')),
        (v.ImgOverlay = (0, m.default)('card-img-overlay'));
      var b = v;
      (t.default = b), (e.exports = t.default);
    },
    297: function(e, t, a) {
      'use strict';
      var l = a(3);
      (t.__esModule = !0), (t.default = void 0);
      var n = l(a(0)).default.createContext(null);
      (t.default = n), (e.exports = t.default);
    },
    298: function(e, t, a) {
      'use strict';
      var l = a(3);
      (t.__esModule = !0), (t.default = void 0);
      var n = l(a(29)),
        r = l(a(57)),
        s = l(a(27)),
        c = l(a(0)),
        i = a(174),
        o = c.default.forwardRef(function(e, t) {
          var a = e.bsPrefix,
            l = e.className,
            o = e.variant,
            m = e.as,
            u = void 0 === m ? 'img' : m,
            d = (0, r.default)(e, ['bsPrefix', 'className', 'variant', 'as']),
            f = (0, i.useBootstrapPrefix)(a, 'card-img');
          return c.default.createElement(
            u,
            (0, n.default)(
              { ref: t, className: (0, s.default)(o ? f + '-' + o : f, l) },
              d
            )
          );
        });
      (o.displayName = 'CardImg'), (o.defaultProps = { variant: null });
      var m = o;
      (t.default = m), (e.exports = t.default);
    },
    339: function(e, t, a) {
      'use strict';
      a(44),
        a(38),
        a(13),
        a(24),
        a(30),
        a(100),
        a(45),
        a(19),
        a(20),
        a(58),
        a(39),
        a(5),
        a(6),
        a(1),
        a(9),
        a(33),
        Object.defineProperty(t, '__esModule', { value: !0 }),
        (t.default = void 0);
      var l = (function(e) {
        if (e && e.__esModule) return e;
        var t = {};
        if (null != e)
          for (var a in e)
            if (Object.prototype.hasOwnProperty.call(e, a)) {
              var l =
                Object.defineProperty && Object.getOwnPropertyDescriptor
                  ? Object.getOwnPropertyDescriptor(e, a)
                  : {};
              l.get || l.set ? Object.defineProperty(t, a, l) : (t[a] = e[a]);
            }
        return (t.default = e), t;
      })(a(0));
      function n(e) {
        for (var t = 1; t < arguments.length; t++) {
          var a = null != arguments[t] ? arguments[t] : {},
            l = Object.keys(a);
          'function' == typeof Object.getOwnPropertySymbols &&
            (l = l.concat(
              Object.getOwnPropertySymbols(a).filter(function(e) {
                return Object.getOwnPropertyDescriptor(a, e).enumerable;
              })
            )),
            l.forEach(function(t) {
              r(e, t, a[t]);
            });
        }
        return e;
      }
      function r(e, t, a) {
        return (
          t in e
            ? Object.defineProperty(e, t, {
                value: a,
                enumerable: !0,
                configurable: !0,
                writable: !0,
              })
            : (e[t] = a),
          e
        );
      }
      function s(e, t) {
        if (null == e) return {};
        var a,
          l,
          n = (function(e, t) {
            if (null == e) return {};
            var a,
              l,
              n = {},
              r = Object.keys(e);
            for (l = 0; l < r.length; l++)
              (a = r[l]), t.indexOf(a) >= 0 || (n[a] = e[a]);
            return n;
          })(e, t);
        if (Object.getOwnPropertySymbols) {
          var r = Object.getOwnPropertySymbols(e);
          for (l = 0; l < r.length; l++)
            (a = r[l]),
              t.indexOf(a) >= 0 ||
                (Object.prototype.propertyIsEnumerable.call(e, a) &&
                  (n[a] = e[a]));
        }
        return n;
      }
      function c(e) {
        return (c =
          'function' == typeof Symbol && 'symbol' == typeof Symbol.iterator
            ? function(e) {
                return typeof e;
              }
            : function(e) {
                return e &&
                  'function' == typeof Symbol &&
                  e.constructor === Symbol &&
                  e !== Symbol.prototype
                  ? 'symbol'
                  : typeof e;
              })(e);
      }
      function i(e, t) {
        for (var a = 0; a < t.length; a++) {
          var l = t[a];
          (l.enumerable = l.enumerable || !1),
            (l.configurable = !0),
            'value' in l && (l.writable = !0),
            Object.defineProperty(e, l.key, l);
        }
      }
      function o(e, t) {
        return !t || ('object' !== c(t) && 'function' != typeof t)
          ? (function(e) {
              if (void 0 === e)
                throw new ReferenceError(
                  "this hasn't been initialised - super() hasn't been called"
                );
              return e;
            })(e)
          : t;
      }
      function m(e) {
        return (m = Object.setPrototypeOf
          ? Object.getPrototypeOf
          : function(e) {
              return e.__proto__ || Object.getPrototypeOf(e);
            })(e);
      }
      function u(e, t) {
        return (u =
          Object.setPrototypeOf ||
          function(e, t) {
            return (e.__proto__ = t), e;
          })(e, t);
      }
      var d = (function(e) {
        function t(e) {
          var a;
          return (
            (function(e, t) {
              if (!(e instanceof t))
                throw new TypeError('Cannot call a class as a function');
            })(this, t),
            ((a = o(this, m(t).call(this, e))).state = {
              humanInteraction: !1,
            }),
            a
          );
        }
        var a, r, d;
        return (
          (function(e, t) {
            if ('function' != typeof t && null !== t)
              throw new TypeError(
                'Super expression must either be null or a function'
              );
            (e.prototype = Object.create(t && t.prototype, {
              constructor: { value: e, writable: !0, configurable: !0 },
            })),
              t && u(e, t);
          })(t, l.Component),
          (a = t),
          (r = [
            {
              key: 'createContactLink',
              value: function(e) {
                var t, a;
                if (e.email)
                  (t = 'mailto:'.concat(e.email)),
                    e.headers &&
                      (t += '?'.concat(
                        ((a = e.headers),
                        Object.keys(a)
                          .map(function(e) {
                            return ''
                              .concat(e, '=')
                              .concat(encodeURIComponent(a[e]));
                          })
                          .join('&'))
                      ));
                else if (e.tel) t = 'tel:'.concat(e.tel);
                else if (e.sms) t = 'sms:'.concat(e.sms);
                else if (e.facetime) t = 'facetime:'.concat(e.facetime);
                else if (e.href) t = e.href;
                else {
                  if ('object' === c(e.children)) return '';
                  t = e.children;
                }
                return t;
              },
            },
            {
              key: 'handleClick',
              value: function(e) {
                var t = this.props.onClick;
                !1 === this.state.humanInteraction &&
                  (e.preventDefault(),
                  t && 'function' == typeof t && t(),
                  (window.location.href = this.createContactLink(this.props)));
              },
            },
            {
              key: 'handleCopiability',
              value: function() {
                this.setState({ humanInteraction: !0 });
              },
            },
            {
              key: 'reverse',
              value: function(e) {
                if (void 0 !== e)
                  return e
                    .split('')
                    .reverse()
                    .join('')
                    .replace('(', ')')
                    .replace(')', '(');
              },
            },
            {
              key: 'render',
              value: function() {
                var e = this.state.humanInteraction,
                  t = this.props,
                  a = t.element,
                  r = void 0 === a ? 'a' : a,
                  i = t.children,
                  o = t.tel,
                  m = t.sms,
                  u = t.facetime,
                  d = t.email,
                  f = t.href,
                  h = (t.headers, t.obfuscate),
                  E = t.obfuscateChildren,
                  p = t.linkText,
                  v = t.style,
                  b = s(t, [
                    'element',
                    'children',
                    'tel',
                    'sms',
                    'facetime',
                    'email',
                    'href',
                    'headers',
                    'obfuscate',
                    'obfuscateChildren',
                    'linkText',
                    'style',
                  ]),
                  g = i || o || m || u || d || f,
                  N = n({}, v || {}, {
                    unicodeBidi: 'bidi-override',
                    direction: !0 === e || !1 === h || !1 === E ? 'ltr' : 'rtl',
                  }),
                  y =
                    !0 === e || !1 === h || 'object' === c(i) || !1 === E
                      ? g
                      : this.reverse(g),
                  w =
                    'a' === r
                      ? {
                          href:
                            !0 === e || !1 === h
                              ? this.createContactLink(this.props)
                              : p || 'obfuscated',
                          onClick: this.handleClick.bind(this),
                        }
                      : {},
                  k = n(
                    {
                      onFocus: this.handleCopiability.bind(this),
                      onMouseOver: this.handleCopiability.bind(this),
                      onContextMenu: this.handleCopiability.bind(this),
                    },
                    b,
                    w,
                    { style: N }
                  );
                return l.default.createElement(r, k, y);
              },
            },
          ]) && i(a.prototype, r),
          d && i(a, d),
          t
        );
      })();
      t.default = d;
    },
  },
]);
//# sourceMappingURL=component---src-pages-resume-jsx-ebf49188630986db365f.js.map
