/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */


// Apply styles across the site neatly using client side rendering

// CAUTION: Pulls images in a little slow but insures little other than them
// are moving off server, saving time && bandwidth

import "./src/styles/main.scss";

