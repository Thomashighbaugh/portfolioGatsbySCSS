---
title: SaarJanak
date: '2019-08-05'
description: An abstract background generator written in a short vanilla JS file that both provides a web app for background generation and demonstrates competence with JS in general.
tags: html5, css3, resurgens, portfolio, design
---

### Using JS for Image Generation

## Links
<div>
   <a href="https://github.com/Thomashighbaugh/manjaro-workstation-playbook">
   [workstation]
   </a>
   <br/>
   <a href="https://github.com/Thomashighbaugh/manjaro-hypervisor-playbook">
   [hypervisor]
   </a>
   <hr/>
</div>
## Purpose

The purpose of this project was to create custom images with Javascript, and thus
begin the process of generating graphics and images with code in general.

## Inspiration

I was inspired to begin thinking of this project by the project "HeroBackgrounds" which
generates custom backgrounds with user inputted colors and custom SVG templates
the user may select from on the project's site.

## Notes on the Source Code

The application utilizes rather simple logic to create a custom background using
Javascript. It is a rather simple piece that I put together quickly and only accepts
one color that it then produces an image with gradient. The coordinates of the gradient are
randomized as is the color that is used. Users may refresh the application until
the rendered image is sufficient for their purposes.

## Features TODOs

- Add ability for users to select colors
- Add support for multiple colors
- Add restyled interface

## Project's Future

Likely the next time I utilize JS for image generation purposes, I will start
from scratch and attempt to make something more like the project that inspired me
to make this rather simple application. However, I felt it was still valuable
experience and a healthy way to appreciate the immense effort that went into
HeroPatterns. As of yet I have not had sufficient time to draw SVG patterns
to utilize as such
