---
title: Not-Another-Devlog
date: '2019-08-05'
description: The Gatsby/React/SASS developer's blog that was developed with this site and the Neon Noir Galleries site.
image: '../../assets/Opitx.png'
tags: gatsby, devlog, blog, react, sass, ssg
---
### A Site to Vent, A Site to Rant

## Problem 
The power of Gatsby and GraphQL can be utilized for various purposes, but it lends itself 
most organically to the creation of blogs that are easily constructed as the pages are 
rendered from Markdown and then arranged using templates programmed in React using GraphQL
middleware, which is how this page was made for instance. Being that I have a lot to say
about certain subjects, as should be obvious here especially, it would behoove me to have 
a blog to house my rants before I become a Linus Torvalds level jerk from my explorations
of Linux internals and tiling window managers. A blog should answer the following needs:

- Utilize Gatsby to Create A Blog

- Stylesheets Consistent with the One Used in This Site 

- 


<button className="nav-btn  ml-2">
   <a href="https://github.com/Thomashighbaugh/decommisioner">
   [github]
   </a>
</button>
<button className="nav-btn ml-2">
 <a href="https://not-another-devlog.netlify.com/">
   [hosted]
   </a>
</button>
