---
title: ResurgensIII
date: '2019-08-05'
description: The third incarnation of my portfolio site, written mainly in HTML5 and CSS3 with additional JS functionality, the first to feature my artwork within the interface (as the background) and using modals and tabulation as a means of displaying the content in a stylish way,
image: '../../assets/Opitx.png'
tags: html5, css3, resurgens, portfolio, design
---

### Subtitle

![Resurgens III](https://raw.githubusercontent.com/Thomashighbaugh/ThomasLeonHighbaugh-Personal-Site/master/content/assets/resurgensIII.png)

## Problem

What need does this project address

- Create a more stylish portfolio site that has more visual flare than the card based portfolio
  that was Resurgens II
- Utilize third party icons and libraries more effectively in the site
- Feature my artwork as graphics in the site

## Solution

- The background image of the site was a picture of mine that gave a nice `a e s t h e t i c `
glow to the site and tied in the blue and pink tones that I used in the site. Overall this 
site's appearance is one of my favorites in my portfolio due to the graphics and colors. 
Even if I cannot think back to developing it without becoming mentally exhausted at the 
memory of the obnoxious length that single page web applications built in HTML & CSS are like.

- The modal-derived manner of arranging the content worked out quite well, using JS to hide the 
parts of the content not on screen, worked out without too many hiccups in the process and 
a minimum of hair pulled out (in contrast to this site). This overcame the rather simplistic
and boring aspects of the site utilized in the prior incarnation of my portfolio site better
than I expected it to. 

- The use of icons on the tabs that select which content to display looked rather bolted on
and for subsequent projects I should attempt more natural utilization of icons, something 
easier for me now as I can look them up using a rofi function I have since programmed into
my window manager that allows me to browse and copy FontAwesome icons from their complete 
catalogue and my discovery of React icons. Nonetheless, the utilization of these small trinkets
of third party design and my overall preference for utilizing some of the aspects of Bootstrap
that I find useful made for a more ergonomic development experience. 

## Notes on Source Code

- Being that the single page web application paradigm is so essential to so many conventional
  portfolio sites, it seemed unwise to deviate from it but it makes for a nightmare of a
  source file that would only be easy to pick apart with the 'code map as scrollbar' functionality
  of Sublime. Finding that particular IDE irritating, I instead had to settle on using an Emacs
  feature in Spacemacs to replicate that, but still a more modular (ie objective oreinted)
  source code structure would be far easier to debug and work with than the long, obnoxious
  source code that I ended up with.
- The site has one of the better color schemes in this era of my learning and is probably
  the best looking site that I have made that is not in React & SCSS. The way that its arranged
  and formatted is also logical and easy to understand. Even if the source code is the
  jumbled nightmare that single page web applications tend to be.

### Problems Raised by the Site

- Need exists for better responsiveness
- Limited amount of JS in the project, since I am showcasing that skill in addition to
  HTML & CSS skills, I need to incorporate more of it in the next version
- Since I am learning React, using it in the creation of a portfolio site is starting to make
  sense, Resurgens IV should be in React

## Links

<button className="nav-btn  ml-2">
   <a href="https://github.com/Thomashighbaugh/decommisioner">
   [github]
   </a>
</button>
<button className="nav-btn ml-2">
 <a href="https://not-another-devlog.netlify.com/">
   [hosted]
   </a>
</button>
