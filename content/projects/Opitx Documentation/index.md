---
title: Opitx Documentation Site
date: '2019-06-09'
description: Opitx is a minimal markdown editor that I made so I could save my files where I want to on my local directory. The first effort I have made that employs Electron as well as being the second major React project.
tags: electron, react, markdown, desktop, packaging, webpack, prismjs
---

### The Stylish Alternative to Docs Site Boilerplate

![Opitx Documentation Front Page](https://raw.githubusercontent.com/Thomashighbaugh/resurgens-iv/master/Screenshot_2019-08-12_19-35-21.png)

## Problem

While many docs sites have troubles far more essential than dealing with than the 
appearance, like making coherent statements that indicate how to use the program, 
there is none the less a tendency for otherwise well designed and thorough programs 
to have docs sites that are merely boilerplate + cryptic text. Additionally, if an 
effort does not generate a web site like developing BASH scripts or Electron apps, 
as this is the docs site to, I normally host some front end splash page for the
project as a means of sharpening my skills. However, this site is a particular point
of pride for me due to the very appealing and easy to use interface that makes the 
information clear and easily accessible.

## Notes on Source Code
- **BRANDING** - Like the Opitx application, this site features all of my own 
branding that I came up with while developing the application and implemented 
personally. This branding is such that the site and application are consistent 
and congruent in appearance. This follow through in implementation of the 
design, as well as the particular functionality of the design, are why this 
page is being showcased at all.

- **HTML/CSS** - While the application is written in React and SCSS utilizing the 
Electron platform, I wanted to take the time to take a break from React when writing out
the documentation site, so it is written as practice in HTML/CSS. Which worked out well
and was a helpful exercise that is part of the consistent practice that all of these skills
require due to their perishable natures. 

<button className="nav-btn">
   <a href="https://github.com/Thomashighbaugh/Opitx-Docs-Site">
   [github]
   </a>
</button>
<button className="nav-btn ">
   <a href="https://opitx-docs.netlify.com/">
   [github]
   </a>
</button>
