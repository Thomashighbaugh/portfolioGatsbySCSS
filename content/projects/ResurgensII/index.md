---
title: ResurgensII
date: '2019-08-05'
description: The second of my portfolio sites, written mainly in HTML5 and CSS3, the first to feature my design acumen coming into its own while also maintaining a simple to use and understand interface.
image: '../../assets/Opitx.png'
tags: html5, css3, resurgens, portfolio, design
---

### Subtitle

![image](url)

## Problem

For the second incarnation of my portfolio site, I wanted to address the following points:

- Incorporate JS
- Minimalistic Design featuring contrasting greys and vibrant colors
- Express growing and specific sense of functional design within the context of a single web application

## Solution

To check off these boxes, I had the following experiences

- To utilize JS within the project, I settled on using it for the transitions and card effects that the user hovering over the cards would trigger. Incorporating JS in this context, where it is not a mission critical aspect of the site but is also not insignificant, proved itself to be a useful way to begin incorporating the power of client side rendering into my efforts more experimentally.
- I settled on a design that focused on cards that held some topical aggregation of data I wanted to display. This enabled the maximization of the screen real estate typically available to a desktop user and even enabled more responsiveness than I originally assumed would be possible as the cards are easily consolidated into fewer rows.
- The color scheme utilized was probably the best part of this particular effort as it expressed the desired contrast between a muted gray tone and a series of vibrant colors specific to each card. The interface did offer me a means of honing my budding design skills, especially in the merging of the site's functionality with its design instead of the typical method of wrapping up the uglier parts with gimmicks to mask them as is often the case with portfolio sites I typically see when browsing GitHub or GitLab for inspiration.

## Notes on Source Code
- First interface in the Electric Tantra style, the genre that has come to be synonymous with 
- Follows the single page web application model popular with developer portfolio sites without being yet another carbon copy of some mildly hideous template.
- Earliest example of one of my portfolio sites that remains interesting as a matter of understanding my trajectory as an autodidactdddddd 
## Links

 <button style="width:45%;" className="nav-btn  ml-2">
    <a href="https://github.com/Thomashighbaugh/decommisioner">
    github
    </a>
 </button>
 <button style="width:45%;" className="nav-btn ml-2">
  <a href="https://not-another-devlog.netlify.com/">
    hosted
    </a>
 </button>           
