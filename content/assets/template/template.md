---
title: Template 
date: '2019-00-00'
description: Project description string
tags: 
---
### Subtitle
![image](url)
## Links
<button className="nav-btn  ml-2">
   <a href="https://github.com/Thomashighbaugh/decommisioner">
   [github]
   </a>
</button>
<button className="nav-btn ml-2">
 <a href="https://not-another-devlog.netlify.com/">
   [hosted]
   </a>
</button>

## Problem
What need does this project address

## Solution
How this project and its underlying technology address the problem

## Notes on Source Code
- things that would be of interest to anyone looking
- over the source code
